package com.sensedia.notification.adapters.amqp.config;

public class EventConfig {

  public static final String NOTIFICATION_OPERATION_ERROR_EVENT_NAME = "NotificationOperationError";

  private EventConfig() {}
}
