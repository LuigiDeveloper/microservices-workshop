package com.sensedia.notification.adapters.amqp;

import com.sensedia.commons.errors.resolvers.ExceptionResolver;
import com.sensedia.notification.adapters.amqp.config.BindConfig;
import com.sensedia.notification.adapters.amqp.config.BrokerInput;
import com.sensedia.notification.adapters.dtos.CustomerAccountCreationDto;
import com.sensedia.notification.adapters.mappers.NotificationMapper;
import com.sensedia.notification.domains.Notification;
import com.sensedia.notification.ports.AmqpPort;
import com.sensedia.notification.ports.ApplicationPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(BrokerInput.class)
@Slf4j
public class AmqpNotificationAdapterInbound {

    private final ApplicationPort applicationPort;
    private final NotificationMapper notificationMapper;
    private final AmqpPort amqpPort;
    private final ExceptionResolver exceptionResolver;

    public AmqpNotificationAdapterInbound(
            ApplicationPort applicationPort,
            NotificationMapper notificationMapper, AmqpPort amqpPort, ExceptionResolver exceptionResolver) {
        this.applicationPort = applicationPort;
        this.notificationMapper = notificationMapper;
        this.amqpPort = amqpPort;
        this.exceptionResolver = exceptionResolver;
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_ACCOUNT_CREATED)
    public void subscribeExchangeAccountCreation(CustomerAccountCreationDto customerCreationDto) {
        try {
            log.info("Account and customer received: " + customerCreationDto.toString());
            Notification notification = this.notificationMapper.toNotification(customerCreationDto);
            this.applicationPort.notify(notification);
        } catch (Exception e) {
            log.error("Error in amqp port inbound notification creation requested: " + e.getMessage());
            amqpPort.notifyNotificationOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(customerCreationDto));
        }
    }
}
