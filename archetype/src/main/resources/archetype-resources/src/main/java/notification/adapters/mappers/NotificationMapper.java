package ${package}.notification.adapters.mappers;

import ${package}.notification.adapters.dtos.CustomerAccountCreationDto;
import ${package}.notification.domains.Notification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NotificationMapper {

    @Mapping(target = "firstName", source = "customer.firstName")
    @Mapping(target = "lastName", source = "customer.lastName")
    Notification toNotification(CustomerAccountCreationDto customerCreationDto);
}
